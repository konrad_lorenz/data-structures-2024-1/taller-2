# Taller Avanzado de Estructuras de Datos: Multiplicación de Matrices y Tensores

## Objetivo del Taller
Este taller tiene como objetivo profundizar en el entendimiento y la aplicación de estructuras de datos complejas, como matrices y tensores, a través de la implementación de algoritmos de multiplicación de matrices que demanden alta capacidad de cálculo. Utilizando Python en Google Colab, los participantes experimentarán con diferentes entornos de ejecución (CPU, GPU) para analizar y comparar el rendimiento. Este enfoque permitirá a los participantes comprender la importancia de la selección del hardware adecuado en la optimización de algoritmos.

## Parte 1: Multiplicación de Matrices en Python

### Objetivo
Implementar un algoritmo de multiplicación de matrices en Python que exija capacidad de cálculo y medir su tiempo de ejecución en diferentes entornos (CPU, GPU, TPU).

### Requisitos

1. **Implementación del Algoritmo**: Escribir un script en Python para multiplicar dos matrices grandes utilizando Numpy o listas de Python. Asegurarse de que las matrices sean lo suficientemente grandes como para poder observar diferencias significativas en el tiempo de ejecución entre los diferentes entornos.
2. **Medición de Tiempo de Ejecución**: Usar Google Colab para ejecutar el script en CPU y luego en GPU. Documentar el tiempo de ejecución en cada caso.

### Recomendaciones para la Prueba con GPU

- Asegurarse de cambiar el entorno de ejecución en Google Colab a GPU (Editar -> Configuración del cuaderno -> Acelerador de hardware -> GPU).
- Utilizar matrices grandes para que la diferencia en el rendimiento sea notable.

## Parte 2: Multiplicación de Tensores con TensorFlow y Análisis de Rendimiento en CPU, GPU y TPU

### Objetivo
Implementar una operación de multiplicación de tensores utilizando TensorFlow y comparar el rendimiento de la ejecución en CPU, GPU y TPU.

### Requisitos

1. **Implementación de la Operación con Tensores**: Utilizar TensorFlow para implementar la multiplicación de dos tensores grandes.
2. **Comparación de Rendimiento**: Ejecutar la operación en CPU, GPU y TPU en Google Colab, midiendo el tiempo de ejecución en cada entorno. 

### Recomendaciones para la Prueba con TPU

- Cambiar el entorno de ejecución en Google Colab a TPU (Editar -> Configuración del cuaderno -> Acelerador de hardware -> TPU).
- Asegurarse de que los datos están correctamente formateados y accesibles para la TPU.
- Utilizar tensores grandes para maximizar el rendimiento de la TPU.

## Entrega de Resultados

- **Tabla Comparativa**: Incluir una tabla comparativa en el informe que muestre los tiempos de ejecución del algoritmo en CPU, GPU y TPU, permitiendo una comparación directa del rendimiento en los diferentes entornos.
- **Informe**: Elaborar un informe en LaTeX que discuta los resultados obtenidos, las conclusiones sobre el rendimiento de los diferentes entornos de ejecución y recomendaciones sobre cuándo usar cada uno. Este informe debe ser conciso y no exceder de dos páginas.
- **Código y Reporte en GitHub/GitLab**: Subir el notebook de Google Colab y el reporte en LaTeX a un repositorio en GitHub o GitLab. Asegurarse de que el repositorio contenga instrucciones claras sobre cómo ejecutar el notebook y visualizar los resultados.

## Evaluación

Los proyectos serán evaluados en base a la correcta implementación de los algoritmos, la calidad y profundidad del análisis de rendimiento, y la claridad y organización del informe final. Se valorará especialmente la capacidad para extraer conclusiones significativas de los datos de rendimiento recopilados y para proporcionar recomendaciones prácticas basadas en estos resultados y estructuras.

Este taller no solo reforzará el conocimiento teórico sobre estructuras de datos y la importancia del cálculo numérico, sino que también ofrecerá una experiencia práctica invaluable en la optimización de algoritmos para diferentes tipos de hardware.


# NOTA 1 (guia):

``` 
resolver = tf.distribute.cluster_resolver.TPUClusterResolver(tpu='')
tf.config.experimental_connect_to_cluster(resolver)
tf.tpu.experimental.initialize_tpu_system(resolver)
strategy = tf.distribute.TPUStrategy(resolver)
```

# NOTA 2: 

> recuerde usar todas las herramientas existentes para resulverlo (llama, gpt, Gemini etc)

